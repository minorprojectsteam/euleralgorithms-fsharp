﻿module Common

open System.Numerics

let rec fibonacci first second max =
    match first + second with 
    | sum when sum <= max -> sum :: fibonacci second sum max 
    | _ -> []

let rec factorise (number : BigInteger) proposed accumulator =
    if proposed = number then proposed::accumulator
    elif number % proposed = 0I then factorise (number/proposed) proposed (proposed::accumulator)
    else factorise number (proposed + 1I) accumulator