module Tests

open System
open Xunit
open Algorithms100

[<Fact>]
let ``Test sum of multiples of 3 and 5`` () =
    let result = ``sum of multiples of 3 and 5`` 1000
    Assert.Equal(result, 233168)

[<Fact>]
let ``Test sum of even fibonacci numbers`` () =
    let result = ``even fibonacci numbers`` 4000000
    Assert.Equal(result, 4613732)

[<Fact>]
let ``test the largest prime factor`` () =
    let result = ``largest prime factor of`` 600851475143I
    Assert.Equal(result, 6857I)